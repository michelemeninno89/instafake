package com.example.instatest.feature.usergallery.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.content.Context
import com.example.instatest.core.NetworkStatus.*
import com.example.instatest.core.Resource
import com.example.instatest.feature.usergallery.model.UserPicture
import com.example.instatest.repository.UserInfoRepository
import com.example.instatest.storage.LocalUserContentRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class UserMediaViewModel @Inject constructor(
    val userInfoRepository: UserInfoRepository,
    val userContentLocalStorage: LocalUserContentRepository
) : ViewModel() {

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()
    private val userMediaResource: Resource<List<UserPicture>> = Resource(EMPTY)
    var currentPosUserGallery: Int = 0
    var userMediaLiveData: MutableLiveData<Resource<List<UserPicture>>> = MutableLiveData()


    fun getUserMedia(accessToken: String, context: Context, mustStoreData: Boolean = true) {
        when (userMediaResource.networkStatus) {
            SUCCESS -> {
                userMediaLiveData.postValue(userMediaResource)
            }
            EMPTY -> {
                userMediaResource.networkStatus = LOADING
                userMediaLiveData.postValue(userMediaResource)
                if (userContentLocalStorage.isOfflineAndDataIsStored(context)) {
                    userContentLocalStorage.readUserInfoAsync(context)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            postUserMediaResource(it, context)
                        }, {
                            postErrorWhileRetrievingData(it)
                        }
                        )
                } else {
                    compositeDisposable.add(
                        userInfoRepository.getUserInfo(accessToken)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe({
                                postUserMediaResource(it, context)
                                if(mustStoreData) {
                                    userContentLocalStorage.storeUserInfoAsync(it, context)
                                }
                            }, {
                                postErrorWhileRetrievingData(it)
                            })
                    )
                }
            }
        }
    }

    private fun postErrorWhileRetrievingData(it: Throwable?) {
        userMediaResource.networkStatus = ERROR
        userMediaResource.throwable = it
        userMediaLiveData.postValue(userMediaResource)
    }

    private fun postUserMediaResource(it: List<UserPicture>, context: Context) {
        userMediaResource.networkStatus = SUCCESS
        userMediaResource.data = it
        userMediaLiveData.postValue(userMediaResource)
        userContentLocalStorage.storeUserInfoAsync(it, context)
    }

    fun getUserPictures(): List<UserPicture> {
        if (userMediaResource.networkStatus == SUCCESS) {
            return userMediaResource.data ?: return emptyList()
        }
        return emptyList()
    }

}