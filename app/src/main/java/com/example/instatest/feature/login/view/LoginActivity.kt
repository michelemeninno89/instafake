package com.example.instatest.feature.login.view

import android.arch.lifecycle.ViewModelProvider
import android.content.Intent
import android.os.Bundle
import com.example.instatest.R
import com.example.instatest.core.ActivityBase
import com.example.instatest.feature.usergallery.view.UserGalleryActivity
import com.example.instatest.storage.LocalUserContentRepository
import com.example.instatest.util.AppPreferences
import com.example.instatest.util.TOKEN
import javax.inject.Inject

class LoginActivity : ActivityBase() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var localUserContentRepository: LocalUserContentRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_activity)
        if (localUserContentRepository.readToken(this) == null) {
            goToLoginFrag()
        } else {
            goToUserGalleryActivity()
        }
    }

    private fun goToLoginFrag() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragContainer, LoginFragment.newInstance())
            .commit()
    }

    fun goToCredentialsFrag() {
        supportFragmentManager.beginTransaction().addToBackStack(null)
            .replace(R.id.fragContainer, CredentialsFragment.newInstance())
            .commit()
    }

    fun goToUserGalleryActivity() {
        val intent = Intent()
        intent.setClass(this, UserGalleryActivity::class.java)
        startActivity(intent)
        finish()
    }

}