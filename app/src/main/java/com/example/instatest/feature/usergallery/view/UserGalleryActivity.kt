package com.example.instatest.feature.usergallery.view

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log
import com.example.instatest.R
import com.example.instatest.core.ActivityBase
import com.example.instatest.feature.usergallery.viewmodel.UserMediaViewModel
import com.example.instatest.storage.LocalUserContentRepository
import com.example.instatest.util.CURRENT_POS
import javax.inject.Inject

class UserGalleryActivity : ActivityBase() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    lateinit var localUserContentRepository: LocalUserContentRepository
    lateinit var userMediaViewModel: UserMediaViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.usergallery_activity)
        userMediaViewModel = ViewModelProviders.of(this, viewModelFactory).get(UserMediaViewModel::class.java)
        if (savedInstanceState == null) {
            goToThumbsUserGallery()
        }
    }

    private fun goToThumbsUserGallery() {
        supportFragmentManager.beginTransaction()
            .addToBackStack(UserThumbsGalleryFragment.TAG)
            .add(R.id.fragContainer, UserThumbsGalleryFragment.newInstance())
            .commit()
    }

    fun goToFullScreenUserGallery(currentPos: Int) {
        supportFragmentManager.beginTransaction()
            .addToBackStack(null)
            .replace(R.id.fragContainer, UserGalleryFullScreenFragment.newInstance(currentPos))
            .commit()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        Log.d("OnSaveInstanceCalled", UserGalleryActivity::class.java.simpleName)
        super.onSaveInstanceState(outState)
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount == 1) {
            supportFragmentManager.popBackStack()
        }
        super.onBackPressed()
    }

}