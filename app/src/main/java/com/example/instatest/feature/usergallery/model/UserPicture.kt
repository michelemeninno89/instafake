package com.example.instatest.feature.usergallery.model

data class UserPicture (
    val id: String,
    val width: Int,
    val height: Int,
    val url: String,
    val likes: Int,
    val comments: Int
)