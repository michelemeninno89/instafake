package com.example.instatest.feature.usergallery.view

import android.arch.lifecycle.Observer
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import com.example.instatest.core.NetworkStatus.*
import com.example.instatest.core.Resource
import com.example.instatest.feature.usergallery.model.UserPicture
import com.example.instatest.feature.usergallery.view.adapter.UserThumbsGalleryAdapter
import com.example.instatest.util.*
import com.example.instatest.widget.InstaFakeToolbar
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import com.example.instatest.R


class UserThumbsGalleryFragment : Fragment(), InstaFakeToolbar.InstaFakeToolbarListener {

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()
    private var recyclerView: RecyclerView? = null
    private var messageView: TextView? = null
    private var extraLayer: FrameLayout? = null
    lateinit var parentActivity: UserGalleryActivity
    lateinit var toolbar: InstaFakeToolbar

    override fun onToolbarRightButtonClickListener() {
        compositeDisposable.add(AppPreferences.clear(parentActivity)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                parentActivity.hideLoader()
                parentActivity.clearAndGoToLogin()
            })
        parentActivity.showLoader()
    }

    val userMediaObserver = Observer<Resource<List<UserPicture>>> {
        it?.let {
            when (it.networkStatus) {

                SUCCESS -> {
                    parentActivity.hideLoader()
                    it.data?.let {
                        if (recyclerView?.adapter == null) {
                            recyclerView?.adapter = UserThumbsGalleryAdapter(it, {
                                extraLayer?.visibility = View.VISIBLE
                            }) { currentPos ->
                                parentActivity.userMediaViewModel.currentPosUserGallery = getFirstItemVisibilePos()
                                parentActivity.goToFullScreenUserGallery(currentPos)
                            }
                            scrollToPosition(parentActivity.userMediaViewModel.currentPosUserGallery)
                        }
                    }
                }
                LOADING -> {
                    parentActivity.showLoader()
                }
                ERROR -> {
                    parentActivity.hideLoader()
                    showMessageView(getString(R.string.error_message))
                    setListenerOnMessageView {
                        messageView?.visibility = View.GONE
                        getUserMedia()
                    }

                }
                EMPTY -> {
                    showMessageView(getString(R.string.error_message))
                }
            }

        }
    }

    private fun showMessageView(text: String) {
        messageView?.visibility = View.VISIBLE
        messageView?.text = text
    }

    private fun setListenerOnMessageView(onMessageViewClicked: () -> Unit) {
        messageView?.setOnClickListener {
            onMessageViewClicked()
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is UserGalleryActivity) {
            parentActivity = context
        } else {
            throw Exception("parent must be User Gallery Activity")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val inflate = inflater.inflate(R.layout.user_gallery_fragment, container, false)
        recyclerView = inflate.findViewById(R.id.userGallery)
        extraLayer = inflate.findViewById(R.id.extraLayer)
        toolbar = inflate.findViewById(R.id.toolbar)
        messageView = inflate.findViewById(R.id.messageView)
        return inflate
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar.setToolBarListener(this)
        toolbar.showRightButton()
        toolbar.setRightButtonText(getString(R.string.logout))
        recyclerView?.layoutManager = GridLayoutManager(parentActivity, SPAN_COUNT) as RecyclerView.LayoutManager?
        recyclerView?.addItemDecoration(
            GridSpacingItemDecoration(
                SPAN_COUNT,
                Util.dpToPix(parentActivity, 14.0f).toInt(),
                true
            )
        )
        parentActivity.userMediaViewModel.userMediaLiveData.observe(this, userMediaObserver)
        getUserMedia()
    }

    private fun getUserMedia() {
        val accessToken = parentActivity.localUserContentRepository.readToken(parentActivity)
        accessToken?.let {
            parentActivity.userMediaViewModel.getUserMedia(it, parentActivity)
        }
    }

    override fun onResume() {
        super.onResume()
        scrollToPosition(parentActivity.userMediaViewModel.currentPosUserGallery)
    }

    override fun onPause() {
        super.onPause()
        val findFirstCompletelyVisibleItemPosition = getFirstItemVisibilePos()
        parentActivity.userMediaViewModel.currentPosUserGallery = findFirstCompletelyVisibleItemPosition
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        val findFirstCompletelyVisibleItemPosition = getFirstItemVisibilePos()
        outState.putInt(CURRENT_POS_STATE, findFirstCompletelyVisibleItemPosition)
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        val int = savedInstanceState?.getInt(CURRENT_POS_STATE)
        int?.apply {
            parentActivity.userMediaViewModel.currentPosUserGallery = this
        }
    }

    private fun getFirstItemVisibilePos() =
        (recyclerView?.layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition()

    private fun scrollToPosition(position: Int) {
        (recyclerView?.layoutManager as LinearLayoutManager).scrollToPositionWithOffset(
            position,
            0
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }

    companion object {
        val TAG = UserGalleryFullScreenFragment::class.java.simpleName
        const val SPAN_COUNT = 3
        fun newInstance(): UserThumbsGalleryFragment {
            return UserThumbsGalleryFragment().apply {

            }
        }
    }

}