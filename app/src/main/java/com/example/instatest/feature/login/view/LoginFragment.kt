package com.example.instatest.feature.login.view

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.CardView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.instatest.R
import com.example.instatest.util.Util
import kotlinx.android.synthetic.main.login_fragment.*

class LoginFragment : Fragment() {

    private val loginBtn: CardView
        get() = loginButton

    lateinit var parentActivity: LoginActivity

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is LoginActivity) {
            parentActivity = context
        } else {
            throw Exception("parent must be Login Activity")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.login_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loginBtn.setOnClickListener {
            if (Util.isNetworkConnected(parentActivity)) {
                parentActivity.goToCredentialsFrag()
            } else {
                Toast.makeText(parentActivity, getString(R.string.check_your_internet), Toast.LENGTH_SHORT).show()
            }
        }
    }

    companion object {
        fun newInstance(): LoginFragment {
            return LoginFragment().apply {

            }
        }
    }


}