package com.example.instatest.feature.login.view

import android.annotation.SuppressLint
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import kotlinx.android.synthetic.main.crediental_fragment.*
import android.webkit.WebResourceRequest
import android.os.Build
import android.annotation.TargetApi
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.webkit.WebSettings
import android.widget.Toast
import com.example.instatest.R
import com.example.instatest.core.NetworkStatus
import com.example.instatest.core.Resource
import com.example.instatest.feature.login.viewmodel.LoginViewModel
import com.example.instatest.util.*


class CredentialsFragment : Fragment() {

    lateinit var webView: WebView
    lateinit var parentActivity: LoginActivity
    lateinit var requestUrl: String
    var loginViewModel: LoginViewModel? = null
    private val observer = Observer<Resource<String>> {
        when (it?.networkStatus) {
            NetworkStatus.SUCCESS -> {
                it.data?.let { data: String ->
                    context?.let {
                        AppPreferences.putString(TOKEN, data, it)
                    }
                }
                parentActivity.hideLoader()
                parentActivity.goToUserGalleryActivity()
            }
            NetworkStatus.LOADING -> {
                parentActivity.showLoader()
            }
            NetworkStatus.ERROR -> {
                Toast.makeText(context, it.throwable?.message, Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is LoginActivity) {
            parentActivity = context
        } else {
            throw Exception("parent must be Login Activity")
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun initializeWebView() {
        setUrl()
        webView.settings.javaScriptEnabled = true
        webView.settings.setAppCacheEnabled(false)
        webView.settings.cacheMode = WebSettings.LOAD_NO_CACHE
        webView.loadUrl(requestUrl)
        webView.webViewClient = webViewClient
    }

    private fun setUrl() {
        context?.apply {
            requestUrl =
                INSTAGRAM_BASEURL +
                        AUTH_URL +
                        CLIENT_ID +
                        REDIRECT_URI +
                        REDIRECT_URL +
                        RESPONSE_TYPE
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        loginViewModel = ViewModelProviders.of(this, parentActivity.viewModelFactory).get(LoginViewModel::class.java)
        loginViewModel?.accessTokenLiveData?.observe(this, observer)
        val inflate = inflater.inflate(R.layout.crediental_fragment, container, false)
        webView = inflate.findViewById(R.id.credential_webview)
        return inflate
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initializeWebView()
    }

    private var webViewClient: WebViewClient = object : WebViewClient() {

        @TargetApi(Build.VERSION_CODES.N)
        override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
            if (request?.url.toString().startsWith(REDIRECT_URL)) {
                return true
            }
            return false
        }

        @SuppressWarnings("deprecation")
        override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
            if (url.startsWith(REDIRECT_URL)) {
                return true
            }
            return false
        }

        override fun onPageFinished(view: WebView, url: String) {
            super.onPageFinished(view, url)
            if (url.contains(CODE_PARAM)) {
                val uri = Uri.parse(url)
                val code = uri.getQueryParameter(CODE_PARAM)
                context?.let { context: Context ->
                    code?.let { code: String ->
                        loginViewModel?.getAccessToken(CLIENT_ID, code, REDIRECT_URL, CLIENT_SECRET)
                    }
                }
            } else if (url.contains("?error")) {
                Log.e("access_token", "getting error fetching access token")
                Toast.makeText(parentActivity, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        webView.clearCache(true)
        context?.deleteDatabase("webview.db");
        context?.deleteDatabase("webviewCache.db");
    }

    companion object {
        fun newInstance(): CredentialsFragment {
            return CredentialsFragment()
        }


    }

}