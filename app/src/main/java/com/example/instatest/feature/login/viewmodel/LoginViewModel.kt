package com.example.instatest.feature.login.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.example.instatest.core.NetworkStatus
import com.example.instatest.core.Resource
import com.example.instatest.repository.LoginRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class LoginViewModel @Inject constructor(val loginRepo: LoginRepository) : ViewModel() {

    private val compositeDisposable:CompositeDisposable = CompositeDisposable()
    private val accessTokenResource: Resource<String> = Resource(NetworkStatus.EMPTY)
    var accessTokenLiveData: MutableLiveData<Resource<String>> = MutableLiveData()

    fun getAccessToken(clientId: String, code: String, redirectUrl: String, clientSecret: String) {
        accessTokenResource.networkStatus = NetworkStatus.LOADING
        accessTokenLiveData.postValue(accessTokenResource)
        compositeDisposable.add(loginRepo.getAccessToken(clientId, code, redirectUrl, clientSecret)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                accessTokenResource.networkStatus = NetworkStatus.SUCCESS
                accessTokenResource.data = it.accessToken
                accessTokenLiveData.postValue(accessTokenResource)
            },{
                accessTokenResource.networkStatus = NetworkStatus.ERROR
                accessTokenResource.throwable = it
                accessTokenLiveData.postValue(accessTokenResource)
            }))
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }


}