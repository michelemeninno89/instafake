package com.example.instatest.feature.usergallery.view.adapter

import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.example.instatest.R
import com.example.instatest.feature.usergallery.model.UserPicture
import com.squareup.picasso.Picasso

class FullScreenGalleryAdapter(val pictures: List<UserPicture>) : PagerAdapter() {

    override fun instantiateItem(collection: ViewGroup, position: Int): Any {
        val modelObject = pictures[position]
        val inflater = LayoutInflater.from(collection.context)
        val layout = inflater.inflate(R.layout.user_picture_full_screen, collection, false) as ViewGroup
        val userPicture = layout.findViewById<ImageView>(R.id.userPicture)
        Picasso
            .with(collection.context)
            .load(modelObject.url)
            .into(userPicture)
        collection.addView(layout)
        return layout
    }

    override fun isViewFromObject(p0: View, p1: Any): Boolean {
        return p0 === p1
    }

    override fun getCount(): Int {
        return pictures.size
    }

    override fun destroyItem(collection: ViewGroup, position: Int, view: Any) {
        collection.removeView(view as View)
    }


}