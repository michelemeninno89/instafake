package com.example.instatest.feature.usergallery.view.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.instatest.R
import com.example.instatest.feature.usergallery.model.UserPicture
import com.example.instatest.util.Util
import com.example.instatest.widget.ExpandableCardView
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.Picasso

class UserThumbsGalleryAdapter(
    val pictures: List<UserPicture>,
    val onTransactionStarted: () -> Unit,
    val onUserPictureClick: (currentPos: Int) -> Unit
) :
    RecyclerView.Adapter<UserThumbsGalleryAdapter.UserPhotoViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): UserPhotoViewHolder {
        return UserPhotoViewHolder(LayoutInflater.from(p0.context).inflate(R.layout.user_picture_element, p0, false))
    }

    override fun getItemCount(): Int {
        return pictures.size
    }

    override fun onBindViewHolder(p0: UserPhotoViewHolder, p1: Int) {
        p0.onBind(pictures[p1])
    }

    inner class UserPhotoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            onTransactionStarted()
            cardView.translation {
                onUserPictureClick(adapterPosition)
            }
        }

        var userPicture: ImageView = itemView.findViewById<ImageView>(R.id.userPicture)
        var likes: TextView = itemView.findViewById<TextView>(R.id.likes)
        var comments: TextView = itemView.findViewById<TextView>(R.id.comments)
        var cardView: ExpandableCardView = itemView.findViewById<ExpandableCardView>(R.id.cardView)

        fun onBind(picture: UserPicture) {
            likes.text = picture.likes.toString()
            comments.text = picture.comments.toString()
            if (!Util.isNetworkConnected(itemView.context)) {
                Util.readLocalUserImg(itemView.context, picture.id)?.let {
                    Picasso
                        .with(itemView.context)
                        .load(it)
                        .into(userPicture)
                }
            } else {
                Picasso
                    .with(itemView.context)
                    .load(picture.url)
                    .into(userPicture)
            }
        }

    }
}