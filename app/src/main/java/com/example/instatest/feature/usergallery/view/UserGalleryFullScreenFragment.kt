package com.example.instatest.feature.usergallery.view

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.instatest.R
import com.example.instatest.feature.usergallery.view.adapter.FullScreenGalleryAdapter
import com.example.instatest.util.CURRENT_POS
import kotlinx.android.synthetic.main.user_gallery_full_screen.*

class UserGalleryFullScreenFragment : Fragment() {

    private val userGalleryViewPager: ViewPager
        get() = userGalleryPager

    private var currentPos: Int? = null

    lateinit var parentActivity: UserGalleryActivity

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is UserGalleryActivity) {
            parentActivity = context
        } else {
            throw Exception("parent must be User Gallery Activity")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val inflate = inflater.inflate(R.layout.user_gallery_full_screen, container, false)
        return inflate
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val userPictures = parentActivity.userMediaViewModel.getUserPictures()
        if (userPictures.isNotEmpty()) {
            val fullScreenGalleryAdapter = FullScreenGalleryAdapter(userPictures)
            userGalleryViewPager.adapter = fullScreenGalleryAdapter
            arguments?.apply {
                if (this.containsKey(CURRENT_POS)) {
                    currentPos = this.getInt(CURRENT_POS)
                    userGalleryViewPager.currentItem = currentPos!!
                }
            }
        }
    }

    companion object {
        fun newInstance(currentPos: Int): UserGalleryFullScreenFragment {
            return UserGalleryFullScreenFragment().apply {
                val bundle = Bundle()
                bundle.putInt(CURRENT_POS, currentPos)
                arguments = bundle
            }
        }
    }

}