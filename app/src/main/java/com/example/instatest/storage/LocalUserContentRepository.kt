package com.example.instatest.storage

import android.content.Context
import android.content.ContextWrapper
import android.graphics.BitmapFactory
import android.graphics.Bitmap
import android.util.Log
import com.example.instatest.feature.usergallery.model.UserPicture
import com.example.instatest.util.AppPreferences
import com.example.instatest.util.TOKEN
import com.example.instatest.util.USER_GALLERY
import com.example.instatest.util.Util
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.io.IOException
import java.net.URL
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import javax.inject.Inject
import javax.inject.Singleton
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken


open class LocalUserContentRepository {

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    open fun readToken(context: Context): String? {
        return AppPreferences.getString(TOKEN,context)
    }

    open fun storeUserInfoAsync(userPictures: List<UserPicture>, context: Context) {
        compositeDisposable.add(
            Observable.fromCallable {
                val gson = Gson()
                val json = gson.toJson(userPictures)
                AppPreferences.putString(USER_GALLERY, json, context)
                userPictures.forEach {
                    try {
                        val url = URL(it.url)
                        val image = BitmapFactory.decodeStream(url.openConnection().getInputStream())
                        val bytes = ByteArrayOutputStream()
                        image.compress(Bitmap.CompressFormat.PNG, 100, bytes)
                        val contextWrapper = ContextWrapper(context)
                        val directory = contextWrapper.getDir("media", Context.MODE_PRIVATE)
                        val f = File(directory, it.id + ".png")
                        f.createNewFile()
                        val fo = FileOutputStream(f)
                        fo.write(bytes.toByteArray())
                        fo.close()
                    } catch (e: IOException) {
                        Log.d(TAG, e.message)
                    }
                }
            }.subscribeOn(Schedulers.io())
                .subscribe({
                    compositeDisposable.clear()
                }, {
                    compositeDisposable.clear()
                })
        )
    }

    open fun isOfflineAndDataIsStored(context: Context): Boolean {
        return AppPreferences.containsString(USER_GALLERY, context) && !Util.isNetworkConnected(context)
    }

    open fun readUserInfoAsync(context: Context): Observable<List<UserPicture>> {
        return Observable.fromCallable<List<UserPicture>> {
            val userPictures = AppPreferences.getString(USER_GALLERY, context)
            val gson = Gson()
            val listType = object : TypeToken<List<UserPicture>>() {
            }.type
            return@fromCallable gson.fromJson(userPictures!!, listType)
        }.subscribeOn(Schedulers.io())
    }

    companion object {
        val TAG = LocalUserContentRepository::class.java.simpleName
    }

}