package com.example.instatest.widget

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.CardView
import android.util.AttributeSet
import android.view.View
import android.widget.TextView
import com.example.instatest.R

class InstaFakeToolbar @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    lateinit var rightButton: CardView
    lateinit var rightButtonText: TextView
    lateinit var listner: InstaFakeToolbarListener

    override fun onFinishInflate() {
        super.onFinishInflate()
        rightButton = findViewById(R.id.rightButton)
        rightButtonText = findViewById(R.id.rightButtonText)
        rightButton.setOnClickListener {
            listner.onToolbarRightButtonClickListener()
        }
    }

    fun showRightButton(){
        rightButton.visibility = View.VISIBLE
    }

    fun setRightButtonText(text:String){
        rightButtonText.text = text
    }

    fun setToolBarListener(listener: InstaFakeToolbarListener) {
        this.listner = listener
    }

    interface InstaFakeToolbarListener {
        fun onToolbarRightButtonClickListener() {}

    }
}