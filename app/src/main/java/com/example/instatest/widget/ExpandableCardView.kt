package com.example.instatest.widget

import android.content.Context
import android.support.v4.view.ViewCompat
import android.support.v4.view.ViewPropertyAnimatorListener
import android.support.v7.widget.CardView
import android.util.AttributeSet
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator


class ExpandableCardView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : CardView(context, attrs, defStyleAttr) {


    fun translation(animationEndCallback: () -> Unit) {
        val mWidth = this.resources.displayMetrics.widthPixels
        val mHeight = this.resources.displayMetrics.heightPixels
        val deltaX = x - (mWidth / 2.0f - width / 2)
        val deltaY = y - (mHeight / 2.0f - height )
        ViewCompat.animate(this)
            .scaleX(2.0f)
            .scaleY(2.0f)
            .translationZ(100.0f)
            .translationX(-deltaX)
            .translationY(-deltaY)
            .setInterpolator(AccelerateDecelerateInterpolator())
            .setDuration(200)
            .setListener(object : ViewPropertyAnimatorListener {
                override fun onAnimationCancel(p0: View?) {
                }

                override fun onAnimationStart(p0: View?) {
                }

                override fun onAnimationEnd(p0: View?) {
                    animationEndCallback()
                }


            })
            .start()
    }

}