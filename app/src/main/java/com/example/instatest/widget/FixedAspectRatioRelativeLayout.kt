package com.example.instatest.widget

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import com.example.instatest.R


class FixedAspectRatioRelativeLayout @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    private var width: Int? = null
    private var height: Int? = null
    private var widthEqualsHeight: Boolean = false

    init {
        attrs?.apply {
            val obtainStyledAttributes = context.obtainStyledAttributes(this, R.styleable.fixed_aspectratio)
            width = obtainStyledAttributes.getInteger(R.styleable.fixed_aspectratio_fixed_width, 1)
            height = obtainStyledAttributes.getInteger(R.styleable.fixed_aspectratio_fixed_height, 1)
            widthEqualsHeight =
                obtainStyledAttributes.getBoolean(R.styleable.fixed_aspectratio_width_equals_height, false)
            obtainStyledAttributes.recycle()
        }

    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val originalWidth = View.MeasureSpec.getSize(widthMeasureSpec)
        val originalHeight = View.MeasureSpec.getSize(heightMeasureSpec)
        var finalWidth: Int = originalWidth
        var finalHeight: Int = originalWidth
        width?.let { width: Int ->
            height?.let { height: Int ->
                val calculatedHeight = originalWidth * height / width
                if (calculatedHeight > originalHeight) {
                    finalWidth = originalHeight * width / height
                    finalHeight = originalHeight
                } else {
                    finalWidth = originalWidth
                    finalHeight = calculatedHeight
                }
            }
        }

        if (!widthEqualsHeight) {
            super.onMeasure(
                View.MeasureSpec.makeMeasureSpec(finalWidth, View.MeasureSpec.EXACTLY),
                View.MeasureSpec.makeMeasureSpec(finalHeight, View.MeasureSpec.EXACTLY)
            )
        } else {
            super.onMeasure(
                View.MeasureSpec.makeMeasureSpec(originalWidth, View.MeasureSpec.EXACTLY),
                View.MeasureSpec.makeMeasureSpec(originalWidth, View.MeasureSpec.EXACTLY)
            )
        }
    }

}