package com.example.instatest.util

import android.content.Context
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

class AppPreferences {

    companion object {

        fun containsString(key: String, context: Context): Boolean {
            val sharedPreferences = context.getSharedPreferences(APP_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE)
            return sharedPreferences.contains(key)
        }

        fun getString(key: String, context: Context): String? {
            val sharedPreferences = context.getSharedPreferences(APP_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE)
            return sharedPreferences.getString(key, null)
        }

        fun putString(key: String, value: String, context: Context) {
            val sharedPreferences = context.getSharedPreferences(APP_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            editor.putString(key, value)
            editor.apply()
        }

        fun clear(context: Context):Observable<Unit> {
           return Observable.fromCallable<Unit> {
                val sharedPreferences = context.getSharedPreferences(APP_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE)
                val editor = sharedPreferences.edit()
                editor.clear()
                editor.commit()
            }.subscribeOn(Schedulers.io())
        }
    }

}