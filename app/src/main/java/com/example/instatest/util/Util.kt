package com.example.instatest.util

import android.content.Context
import android.content.ContextWrapper
import android.util.TypedValue
import android.net.NetworkCapabilities
import android.net.ConnectivityManager
import android.os.Build
import android.util.Log
import com.example.instatest.storage.LocalUserContentRepository
import java.io.File
import java.io.IOException


class Util {

    companion object {

        fun dpToPix(context: Context, dp: Float): Float {
            val r = context.resources
            return TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dp,
                r.displayMetrics
            )
        }

        fun isNetworkConnected(context: Context): Boolean {
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            if (Build.VERSION.SDK_INT < 23) {
                val ni = cm.activeNetworkInfo

                if (ni != null) {
                    return ni.isConnected && (ni.type == ConnectivityManager.TYPE_WIFI || ni.type == ConnectivityManager.TYPE_MOBILE)
                }
            } else {
                val n = cm.activeNetwork

                if (n != null) {
                    val nc = cm.getNetworkCapabilities(n)
                    return nc.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) || nc.hasTransport(
                        NetworkCapabilities.TRANSPORT_WIFI
                    )
                }
            }
            return false
        }

        fun readLocalUserImg(context: Context, id: String): File? {
            var f: File? = null
            try {
                val contextWrapper = ContextWrapper(context)
                val directory = contextWrapper.getDir("media", Context.MODE_PRIVATE)
                f = File(directory, "$id.png")
            } catch (e: IOException) {
                Log.d(LocalUserContentRepository.TAG, e.message)
            }
            return f
        }
    }

}