package com.example.instatest.util


const val APP_PREFERENCES_FILE_NAME = "userdata"
const val CODE = "token"

//login constants
const val CLIENT_SECRET = "efcc162c80be46459667dd547c92dff7"
const val CLIENT_ID = "59f9541af9794b71b99ecdb3f5679f03"
const val INSTAGRAM_BASEURL = "https://api.instagram.com/"
const val REDIRECT_URL = "https://instagram.com/"
const val AUTH_URL = "oauth/authorize/?client_id="
const val RESPONSE_TYPE = "&response_type=code"
const val REDIRECT_URI = "&redirect_uri="
const val CODE_PARAM = "code"
const val TOKEN = "TOKEN"
//user gallery constants
const val CURRENT_POS = "CURRENT_POS"
const val CURRENT_POS_STATE = "CURRENT_POS_STATE"
const val USER_GALLERY = "USER_GALLERY"

