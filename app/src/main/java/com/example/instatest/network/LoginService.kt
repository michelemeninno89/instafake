package com.example.instatest.network

import com.example.instatest.network.response.AccessTokenResponse
import io.reactivex.Observable
import okhttp3.RequestBody
import retrofit2.http.Body
import retrofit2.http.POST

interface LoginService {
    @POST("oauth/access_token")
    fun getAuthToken(@Body body: RequestBody): Observable<AccessTokenResponse>

}