package com.example.instatest.network

import com.example.instatest.network.response.UserInfoResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface UserMediaService {

    @GET("v1/users/self/media/recent/")
    fun getUserMedia(@Query("access_token") accessToken: String): Observable<UserInfoResponse>

}