package com.example.instatest.network.response

import com.example.instatest.network.response.userinfo.SingleData
import com.google.gson.annotations.SerializedName

data class UserInfoResponse(@SerializedName("data") val data: List<SingleData>)