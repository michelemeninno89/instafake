package com.example.instatest.network.response.userinfo

import com.google.gson.annotations.SerializedName

data class ThumbNail(@SerializedName("width")val width:Int,@SerializedName("height") val height:Int,@SerializedName("url") val url:String )