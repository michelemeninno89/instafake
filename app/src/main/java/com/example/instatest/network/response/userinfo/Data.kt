package com.example.instatest.network.response.userinfo

import com.google.gson.annotations.SerializedName

data class SingleData(
    @SerializedName("id") val id: String,
    @SerializedName("images") val images: Images,
    @SerializedName("likes") val likes: Likes,
    @SerializedName("comments") val comments: Comments
)