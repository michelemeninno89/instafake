package com.example.instatest.network.response.userinfo

import com.google.gson.annotations.SerializedName

data class Comments(@SerializedName("count") val count: Int)