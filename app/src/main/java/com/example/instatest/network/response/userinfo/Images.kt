package com.example.instatest.network.response.userinfo

import com.google.gson.annotations.SerializedName

data class Images(@SerializedName("standard_resolution") val thumbnail: ThumbNail)