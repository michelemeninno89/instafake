package com.example.instatest.network.response.userinfo

import com.google.gson.annotations.SerializedName

data class Likes(@SerializedName("count") val count: Int)