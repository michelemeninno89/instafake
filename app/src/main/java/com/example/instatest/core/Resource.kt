package com.example.instatest.core

import android.support.annotation.NonNull
import android.support.annotation.Nullable

data class Resource<T>(@NonNull var networkStatus: NetworkStatus,
                       @Nullable var throwable: Throwable? = null,
                       @Nullable var data: T? = null)