package com.example.instatest.core

interface Mapper<in S, out T> {

    fun map(input: S): T

}