package com.example.instatest.core

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import android.webkit.CookieManager
import android.webkit.ValueCallback
import com.example.instatest.R
import com.example.instatest.feature.login.view.LoginActivity
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.login_activity.*

abstract class ActivityBase:DaggerAppCompatActivity() {

    private val progressBar: View
        get() = prog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.usergallery_activity)
    }

    fun showLoader() {
        progressBar.visibility = View.VISIBLE
    }

    fun hideLoader() {
        progressBar.visibility = View.GONE
    }

    fun clearAndGoToLogin(){
        val cookieManager = CookieManager.getInstance()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            cookieManager.removeAllCookies(ValueCallback {
                //
            })
        }
        else {
            cookieManager.removeAllCookie()
        }
        val intent = Intent()
        intent.setClass(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }
}