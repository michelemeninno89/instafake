package com.example.instatest.core

enum class NetworkStatus{
    SUCCESS, LOADING, ERROR, EMPTY
}