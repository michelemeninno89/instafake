package com.example.instatest.core

import com.example.instatest.di.component.ApplicationComponent
import com.example.instatest.di.component.DaggerApplicationComponent
import com.example.instatest.di.module.LocalUserContentRepoModule
import com.example.instatest.di.module.NetModule
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

class InstaApp : DaggerApplication(){

    lateinit var appComponent: ApplicationComponent

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        appComponent = DaggerApplicationComponent
            .builder()
            .localUserContentRepoModule(LocalUserContentRepoModule())
            .netModule(NetModule("https://api.instagram.com/"))
            .application(this)
            .build()
        appComponent.inject(this)

        return appComponent
    }
}