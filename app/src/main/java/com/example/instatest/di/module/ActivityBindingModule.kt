package com.example.instatest.di.module

import com.example.instatest.feature.login.view.LoginActivity
import com.example.instatest.feature.usergallery.view.UserGalleryActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {

    @ContributesAndroidInjector
    abstract fun bindingMainActivity(): LoginActivity

    @ContributesAndroidInjector
    abstract fun bindingUserGalleryActivity(): UserGalleryActivity
}