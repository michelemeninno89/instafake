package com.example.instatest.di.module

import com.example.instatest.storage.LocalUserContentRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
open class LocalUserContentRepoModule {

    @Provides
    @Singleton
    open fun provideStorageRepo(): LocalUserContentRepository {
        return LocalUserContentRepository()
    }

}