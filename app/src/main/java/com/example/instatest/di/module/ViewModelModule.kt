package com.example.instatest.di.module

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.example.instatest.feature.login.viewmodel.LoginViewModel
import com.example.instatest.feature.usergallery.viewmodel.UserMediaViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {
    @Binds
    abstract fun bindViewModelFactory(factory: DaggerViewModelFactory) : ViewModelProvider.Factory

    @Binds
    @IntoMap
    @KotlinViewModelKey.ViewModelKey(LoginViewModel::class)
    abstract fun bindLoginViewModel(loginViewModel: LoginViewModel): ViewModel

    @Binds
    @IntoMap
    @KotlinViewModelKey.ViewModelKey(UserMediaViewModel::class)
    abstract fun bindUserMediaViewModel(loginViewModel: UserMediaViewModel): ViewModel
}