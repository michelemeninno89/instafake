package com.example.instatest.di.component

import android.app.Application
import com.example.instatest.core.InstaApp
import com.example.instatest.di.module.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [ContextModule::class, AndroidSupportInjectionModule::class, NetModule::class, ActivityBindingModule::class, ViewModelModule::class, LocalUserContentRepoModule::class])
interface ApplicationComponent : AndroidInjector<DaggerApplication> {

    fun inject(instance: InstaApp)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun netModule(netModule: NetModule): Builder
        fun localUserContentRepoModule(repoModule: LocalUserContentRepoModule):Builder
        fun build(): ApplicationComponent
    }
}