package com.example.instatest.repository.mappers

import com.example.instatest.core.Mapper
import com.example.instatest.feature.usergallery.model.UserPicture
import com.example.instatest.network.response.UserInfoResponse

class UserImageMapper:Mapper<UserInfoResponse, List<UserPicture>> {

    override fun map(input: UserInfoResponse): List<UserPicture> {
        return input.data.map {  singleData -> UserPicture(singleData.id, singleData.images.thumbnail.width, singleData.images.thumbnail.height, singleData.images.thumbnail.url, singleData.likes.count, singleData.comments.count) }
    }
}