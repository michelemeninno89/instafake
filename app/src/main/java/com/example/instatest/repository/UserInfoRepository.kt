package com.example.instatest.repository

import com.example.instatest.feature.usergallery.model.UserPicture
import com.example.instatest.network.UserMediaService
import com.example.instatest.repository.mappers.UserImageMapper
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class UserInfoRepository @Inject constructor(val userMediaService: UserMediaService) {

    private val mapper: UserImageMapper = UserImageMapper()

    fun getUserInfo(accessToken: String): Observable<List<UserPicture>> {
        return userMediaService.getUserMedia(accessToken)
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.computation())
            .map { t -> mapper.map(t) }
    }


}