package com.example.instatest.repository

import com.example.instatest.network.LoginService
import com.example.instatest.network.response.AccessTokenResponse
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import okhttp3.MultipartBody

class LoginRepository @Inject constructor(val loginService: LoginService) {

    fun getAccessToken(clientId:String, code: String, redirectUrl:String, clientSecret:String): Observable<AccessTokenResponse> {

        val requestBody = MultipartBody.Builder()
            .setType(MultipartBody.FORM)
            .addFormDataPart("client_secret", clientSecret)
            .addFormDataPart("client_id", clientId)
            .addFormDataPart("grant_type", "authorization_code")
            .addFormDataPart("redirect_uri",redirectUrl)
            .addFormDataPart("code", code)
            .build()
        return loginService
            .getAuthToken(requestBody)
            .subscribeOn(Schedulers.io())
    }


}