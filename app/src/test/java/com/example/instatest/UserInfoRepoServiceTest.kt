package com.example.instatest

import com.example.instatest.feature.usergallery.model.UserPicture
import com.example.instatest.network.UserMediaService
import com.example.instatest.network.response.UserInfoResponse
import com.example.instatest.network.response.userinfo.*
import com.example.instatest.repository.UserInfoRepository
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import org.junit.Before
import org.junit.Test
import org.mockito.MockitoAnnotations
import retrofit2.Retrofit
import retrofit2.mock.BehaviorDelegate
import retrofit2.mock.MockRetrofit
import retrofit2.mock.NetworkBehavior
import java.util.concurrent.TimeUnit

class UserInfoRepoServiceTest {

    private val behavior = NetworkBehavior.create()
    private val testSubscriberService: TestObserver<UserInfoResponse> = TestObserver.create<UserInfoResponse>()
    private val testSubscriberMappedData: TestObserver<List<UserPicture>> = TestObserver.create<List<UserPicture>>()
    private var mockService: UserMediaService? = null
    lateinit var userRepository: UserInfoRepository

    inner class UserMediaMockedService(private val delegate: BehaviorDelegate<UserMediaService>) : UserMediaService {
        override fun getUserMedia(accessToken: String): Observable<UserInfoResponse> {

            val data = ArrayList<SingleData>()
            data.add(SingleData("1", Images(ThumbNail(1,1,"test")), Likes(3), Comments(4)))
            val userInfoResponse = UserInfoResponse(data)
            return delegate.returningResponse(userInfoResponse).getUserMedia("123")
        }
    }

    @Before
    @Throws(Exception::class)
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        val retrofit = Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl("http://www.sample.com").build()

        val mockRetrofit = MockRetrofit.Builder(retrofit)
            .networkBehavior(behavior).build()
        val delegate = mockRetrofit.create<UserMediaService>(UserMediaService::class.java)

        mockService = UserMediaMockedService(delegate)
        mockService?.let {
            userRepository = UserInfoRepository(it)
        }
    }

    @Test
    fun test_service_success_response() {
        givenNetworkFailurePercentIs(0)
        mockService?.getUserMedia("123")?.subscribeWith(testSubscriberService)
        testSubscriberService.assertComplete()
    }

    @Test
    fun test_success_userData_response() {
        givenNetworkFailurePercentIs(0)
        mockService?.getUserMedia("123")?.subscribeWith(testSubscriberService)
        testSubscriberService.assertValue { t -> (t.data[0].id=="1" && t.data[0].likes.count==3 &&t.data[0].comments.count==4)}
    }

    @Test
    fun test_failure_userData_response() {
        givenNetworkFailurePercentIs(100)
        testSubscriberService.assertNoValues()
        testSubscriberService.assertNotComplete()
    }

    @Test
    fun test_repo_success_response() {
        givenNetworkFailurePercentIs(0)
        userRepository.getUserInfo("").subscribeWith(testSubscriberMappedData)
        testSubscriberMappedData.awaitTerminalEvent()
        testSubscriberMappedData.assertValue { t ->  (t[0].id=="1" && t[0].likes==3 &&t[0].comments==4) }
    }

    @Test
    fun test_repo_failure_response() {
        givenNetworkFailurePercentIs(100)
        userRepository.getUserInfo("").subscribeWith(testSubscriberMappedData)
        testSubscriberMappedData.assertNoValues()
        testSubscriberMappedData.assertNotComplete()
    }

    private fun givenNetworkFailurePercentIs(failurePercent: Int) {
        behavior.setDelay(0, TimeUnit.MILLISECONDS)
        behavior.setVariancePercent(0)
        behavior.setFailurePercent(failurePercent)
    }
}