package com.example.instatest

import com.example.instatest.network.LoginService
import com.example.instatest.network.response.AccessTokenResponse
import com.example.instatest.repository.LoginRepository
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import org.junit.Before
import org.junit.Test
import okhttp3.RequestBody
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import retrofit2.mock.NetworkBehavior
import retrofit2.mock.MockRetrofit
import retrofit2.Retrofit
import retrofit2.mock.BehaviorDelegate
import java.util.concurrent.TimeUnit


class LoginRepoServiceTest {

    private val behavior = NetworkBehavior.create()
    private val testSubscriber: TestObserver<AccessTokenResponse> = TestObserver.create<AccessTokenResponse>()
    private var mockService: LoginService? = null
    lateinit var loginRepository: LoginRepository
    @Mock
    lateinit var requesteBodyMock: RequestBody

    inner class LoginServiceMock(private val delegate: BehaviorDelegate<LoginService>) : LoginService {
        override fun getAuthToken(body: RequestBody): Observable<AccessTokenResponse> {
            return delegate.returningResponse(AccessTokenResponse("123")).getAuthToken(requesteBodyMock)
        }
    }

    @Before
    @Throws(Exception::class)
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        val retrofit = Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl("http://www.sample.com").build()

        val mockRetrofit = MockRetrofit.Builder(retrofit)
            .networkBehavior(behavior).build()
        val delegate = mockRetrofit.create<LoginService>(LoginService::class.java!!)

        mockService = LoginServiceMock(delegate)
        mockService?.let {
            loginRepository = LoginRepository(it)
        }
    }

    @Test
    fun test_service_success_response() {
        givenNetworkFailurePercentIs(0)
        mockService?.getAuthToken(requesteBodyMock)?.subscribeWith(testSubscriber)
        testSubscriber.assertComplete()
    }

    @Test
    fun test_success_tokenValue() {
        givenNetworkFailurePercentIs(0)
        mockService?.getAuthToken(requesteBodyMock)?.subscribeWith(testSubscriber)
        testSubscriber.assertValue { t -> t.accessToken == "123" }
    }

    @Test
    fun test_failure_response() {
        givenNetworkFailurePercentIs(100)
        testSubscriber.assertNoValues()
        testSubscriber.assertNotComplete()
    }

    @Test
    fun test_repo_success_response() {
        givenNetworkFailurePercentIs(0)
        loginRepository.getAccessToken("", "", "", "").subscribeWith(testSubscriber)
        testSubscriber.awaitTerminalEvent()
        testSubscriber.assertValue { t -> t.accessToken == "123" }
    }

    @Test
    fun test_repo_failure_response() {
        givenNetworkFailurePercentIs(100)
        loginRepository.getAccessToken("", "", "", "").subscribeWith(testSubscriber)
        testSubscriber.assertNoValues()
        testSubscriber.assertNotComplete()
    }

    private fun givenNetworkFailurePercentIs(failurePercent: Int) {
        behavior.setDelay(0, TimeUnit.MILLISECONDS)
        behavior.setVariancePercent(0)
        behavior.setFailurePercent(failurePercent)
    }

}