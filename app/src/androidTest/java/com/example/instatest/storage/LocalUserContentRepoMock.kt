package com.example.instatest.storage

import android.content.Context

import com.example.instatest.feature.usergallery.model.UserPicture

class LocalUserContentRepoMock : LocalUserContentRepository() {

    var token: String? = null

    override fun readToken(context: Context): String? {
        return token
    }

    override fun storeUserInfoAsync(userPictures: List<UserPicture>, context: Context) {
        //
    }

    override fun isOfflineAndDataIsStored(context: Context): Boolean {
        return false
    }

}