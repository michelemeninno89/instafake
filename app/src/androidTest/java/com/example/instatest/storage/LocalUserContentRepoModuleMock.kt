package com.example.instatest.storage

import com.example.instatest.di.module.LocalUserContentRepoModule
import com.example.instatest.storage.LocalUserContentRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

class LocalUserContentRepoModuleMock:LocalUserContentRepoModule() {

    override fun provideStorageRepo(): LocalUserContentRepoMock {
        return LocalUserContentRepoMock()
    }

}