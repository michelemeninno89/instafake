package com.example.instatest;

import android.app.Application;
import android.content.Context;
import android.support.test.runner.AndroidJUnitRunner;
import com.example.instatest.core.InstaApp;

public class CustomRunner extends AndroidJUnitRunner {

    @Override
    public Application newApplication(ClassLoader cl, String className, Context context) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
        return super.newApplication(cl, InstaAppFake.class.getName(), context);
    }
}