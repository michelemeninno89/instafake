package com.example.instatest

import com.example.instatest.api.NetModuleMock
import com.example.instatest.di.component.ApplicationComponent
import com.example.instatest.di.module.LocalUserContentRepoModule
import com.example.instatest.storage.LocalUserContentRepoMock
import com.example.instatest.storage.LocalUserContentRepoModuleMock
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

class InstaAppFake : DaggerApplication(){

    lateinit var appComponent: ApplicationComponent

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        appComponent = DaggerTestAppComponent
            .builder()
            .localUserContentRepoModule(LocalUserContentRepoModuleMock())
            .netModule(NetModuleMock("https://api.instagram.com/"))
            .build()
        appComponent.inject(this)
        return appComponent
    }
}