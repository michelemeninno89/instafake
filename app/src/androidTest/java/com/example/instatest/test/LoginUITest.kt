package com.example.instatest.test

import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.support.test.espresso.Espresso
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.example.instatest.feature.login.view.LoginActivity
import android.support.test.espresso.action.ViewActions.*
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import com.example.instatest.R
import com.example.instatest.core.NetworkStatus
import com.example.instatest.core.Resource
import com.example.instatest.feature.login.viewmodel.LoginViewModel
import org.hamcrest.Matchers.`is`
import org.junit.Assert

@RunWith(AndroidJUnit4::class)
class LoginUITest {
    //must be online

    @get:Rule
    val testRuleLogin: ActivityTestRule<LoginActivity> = ActivityTestRule(LoginActivity::class.java, false, false)
    lateinit var loginViewModel: LoginViewModel

    @Test
    fun test_firstlogin_loginSectionShowed() {
        val intent = Intent()
        testRuleLogin.launchActivity(intent)
        Espresso.onView(ViewMatchers.withId(R.id.loginButton)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun test_loginPressed_credentialSectionShowed() {
        testRuleLogin.launchActivity(null)
        Espresso.onView(ViewMatchers.withId(R.id.loginButton)).perform(click())
        Espresso.onView(ViewMatchers.withId(R.id.credential_webview))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun test_successStatus_tokenReceived() {
        testRuleLogin.launchActivity(null)
        Espresso.onView(ViewMatchers.withId(R.id.loginButton)).perform(click())
        loginViewModel = ViewModelProviders.of(testRuleLogin.activity, testRuleLogin.activity.viewModelFactory)
            .get(LoginViewModel::class.java)
        loginViewModel.getAccessToken("", "", "", "")
        loginViewModel.accessTokenLiveData.observeForever { t: Resource<String>? ->
            run {
                if (t!!.networkStatus == NetworkStatus.SUCCESS) {
                    Assert.assertThat(t.data, `is`("123"))
                }
            }
        }
    }
}