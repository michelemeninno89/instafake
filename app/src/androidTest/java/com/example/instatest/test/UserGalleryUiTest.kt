package com.example.instatest.test

import android.arch.lifecycle.ViewModelProviders
import android.support.test.espresso.Espresso
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.rule.ActivityTestRule
import android.support.v7.widget.RecyclerView
import com.example.instatest.feature.usergallery.view.UserGalleryActivity
import com.example.instatest.feature.usergallery.viewmodel.UserMediaViewModel
import com.example.instatest.util.RecyclerViewItemCountAssertion.withItemCount
import org.hamcrest.Matchers
import com.example.instatest.R
import com.example.instatest.core.NetworkStatus
import com.example.instatest.core.Resource
import com.example.instatest.feature.login.view.LoginActivity
import com.example.instatest.feature.usergallery.model.UserPicture
import com.example.instatest.storage.LocalUserContentRepoMock
import org.junit.*

class UserGalleryUiTest {
    @get:Rule
    val testRuleGallery: ActivityTestRule<UserGalleryActivity> =
        ActivityTestRule(UserGalleryActivity::class.java, false, false)
    @get:Rule
    val testRuleLogin: ActivityTestRule<LoginActivity> = ActivityTestRule(LoginActivity::class.java, false, false)

    lateinit var userMediaViewModel: UserMediaViewModel

    @Before
    fun test() {
        testRuleLogin.launchActivity(null)
        (testRuleLogin.activity.localUserContentRepository as LocalUserContentRepoMock).token = "abc"
        testRuleLogin.finishActivity()
    }

    @Test
    fun usergallery_showed_at_start() {
        testRuleGallery.launchActivity(null)
        Espresso.onView(ViewMatchers.withId(R.id.userGallery)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun usergallery_inflated_at_start() {
        testRuleGallery.launchActivity(null)
        userMediaViewModel = ViewModelProviders.of(testRuleGallery.activity, testRuleGallery.activity.viewModelFactory)
            .get(UserMediaViewModel::class.java)
        userMediaViewModel.getUserPictures()
        userMediaViewModel.userMediaLiveData.observeForever { t: Resource<List<UserPicture>>? ->
            run {
                if (t!!.networkStatus == NetworkStatus.SUCCESS) {
                    Espresso.onView(ViewMatchers.withId(R.id.userGallery)).check(withItemCount(Matchers.greaterThan(1)))
                }
            }
        }
    }


    @Test
    fun test_successStatus_galleryReceived() {
        testRuleGallery.launchActivity(null)
        userMediaViewModel = ViewModelProviders.of(testRuleGallery.activity, testRuleGallery.activity.viewModelFactory)
            .get(UserMediaViewModel::class.java)
        userMediaViewModel.getUserPictures()
        // Thread.sleep(150)
        userMediaViewModel.userMediaLiveData.observeForever { t: Resource<List<UserPicture>>? ->
            run {
                if (t!!.networkStatus == NetworkStatus.SUCCESS) {
                    Assert.assertThat(t.data!![0].id, Matchers.`is`("1"))
                }
            }
        }
    }

    @Test
    fun test_pictureClicked_userGalleryFullScreenShowed() {
        testRuleGallery.launchActivity(null)
        userMediaViewModel = ViewModelProviders.of(testRuleGallery.activity, testRuleGallery.activity.viewModelFactory)
            .get(UserMediaViewModel::class.java)
        userMediaViewModel.getUserPictures()
        userMediaViewModel.userMediaLiveData.observeForever { t: Resource<List<UserPicture>>? ->
            run {
                if (t!!.networkStatus == NetworkStatus.SUCCESS) {
                    onView(withId(R.id.userGallery)).perform(
                        actionOnItemAtPosition<RecyclerView.ViewHolder>(
                            0,
                            click()
                        )
                    )
                    Thread.sleep(1200)
                    Espresso.onView(ViewMatchers.withId(R.id.userGalleryPager))
                        .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
                }
            }
        }
    }

    @Test
    fun test_logoutButton_loginSectionShowed() {
        testRuleLogin.launchActivity(null)
        (testRuleLogin.activity.localUserContentRepository as LocalUserContentRepoMock).token = null
        testRuleGallery.launchActivity(null)
        Espresso.onView(ViewMatchers.withId(R.id.rightButton)).perform(click())
        Thread.sleep(1000)
        Espresso.onView(ViewMatchers.withId(R.id.loginButton)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }


}