package com.example.instatest

import com.example.instatest.di.component.ApplicationComponent
import com.example.instatest.di.module.*
import com.example.instatest.test.LoginUITest
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton


@Singleton
@Component(modules = [ContextModule::class, AndroidSupportInjectionModule::class, NetModule::class, ActivityBindingModule::class, ViewModelModule::class, LocalUserContentRepoModule::class])
interface TestAppComponent : ApplicationComponent {
    fun inject(test: LoginUITest)
}