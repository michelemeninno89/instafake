package com.example.instatest.api

import com.example.instatest.di.module.NetModule
import com.example.instatest.network.LoginService
import com.example.instatest.network.UserMediaService
import com.example.instatest.network.response.AccessTokenResponse
import com.example.instatest.network.response.UserInfoResponse
import com.example.instatest.network.response.userinfo.*
import io.reactivex.Observable
import okhttp3.RequestBody
import retrofit2.Retrofit

class NetModuleMock(baseUrl: String) : NetModule(baseUrl) {

    override fun provideLoginService(retrofit: Retrofit): LoginService {
        return object : LoginService {
            override fun getAuthToken(body: RequestBody): Observable<AccessTokenResponse> {
                return Observable.just(AccessTokenResponse("123"))
            }
        }
    }

    override fun provideUserMediaService(retrofit: Retrofit): UserMediaService {
        return object : UserMediaService {
            override fun getUserMedia(accessToken: String): Observable<UserInfoResponse> {
                val listOf = listOf(
                    SingleData(
                        "1",
                        Images(
                            ThumbNail(
                                1,
                                1,
                                "https://it.wikipedia.org/wiki/Wikipedia_in_italiano#/media/File:Wikipedia-logo-v2-it.svg"
                            )
                        ),
                        Likes(3),
                        Comments(4)
                    ),
                    SingleData(
                        "2",
                        Images(
                            ThumbNail(
                                1,
                                1,
                                "https://it.wikipedia.org/wiki/Wikipedia_in_italiano#/media/File:Wikipedia-logo-v2-it.svg"
                            )
                        ),
                        Likes(3),
                        Comments(4)
                    )
                )
                return Observable.just(UserInfoResponse(listOf))
            }

        }
    }
}